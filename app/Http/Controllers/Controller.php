<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function dataSuccess($mes, $data = [],$code = 200)
    {
        return response()->json([
            'result'  => true,
            'message' => $mes,
            'data'    => $data,
            'code'  =>200

        ], $code);
    }
    public function dataSuccessBid($mes, $data = [],$code = 200 , $complete = false)
    {
        return response()->json([
            'result'  => true,
            'message' => $mes,
            'data'    => $data,
            'code'  =>200,
            'complete'  =>$complete,

        ], $code);
    }

    public function dataError($mes, $data = [], $code = 200)
    {
        return response()->json([
            'result'  => false,
            'message' => $mes,
            'data'    => $data,
            'code'    => $code
        ], $code);
    }
    public function dataErrorBids($mes, $price,$data = [], $code = 200)
    {
        return response()->json([
            'result'  => false,
            'message' => $mes,
            'price_next' => $price,
            'data'    => $data,
            'code'    => 200
        ], 200);
    }

    public function convert_slug($str)
    {
          if(!$str) return false;

          $utf8 = array(

          'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

          'd'=>'đ|Đ',

          'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

          'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',

          'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

          'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

          'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

          );

          foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);

          return $str;
    }
}
