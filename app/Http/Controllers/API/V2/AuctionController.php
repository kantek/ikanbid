<?php

namespace App\Http\Controllers\API\V2;

use App\Ad;
use App\Bid;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class AuctionController extends Controller
{
      public function index(Request $request)
      {
            try{
                $auctions = Ad::select("id","title","description","category_id","price","expired_at","next_min","buy_now")->orderby('id','DESC');
                if(@$request->category_id != "")
                {
                    $auctions =  $auctions->where('category_id',$request->category_id);
                }
                if (@$request->name != "")
                {
                    $auctions = $auctions->where('title','like',"%$request->name%");
                }
                $dateNow = date("Y-m-d H:i:s");
                $auctions = $auctions->where('expired_at','>=',$dateNow);
                $auctions = $auctions->paginate(10);

                return $this->dataSuccess('Lấy danh sách đấu giá thành công',$auctions,200);
            }
            catch (\Exception $exception)
            {
                return $this->dataError($exception->getMessage(),null,200);
            }
      }
      public function show($id)
      {
            try{

                $auction = Ad::select("id","title","description","category_id","price","expired_at","next_min","buy_now")->where('id',$id)->first();
                $auction->bids;

                return $this->dataSuccess('Lấy đấu giá chi tiết thành công',$auction,200);

            }
            catch (\Exception $exception)
            {
                return $this->dataError($exception->getMessage(),null,200);
            }
      }

      public function store(Request $request)
      {
          try{
              $auctions = null;
              if($request->price_max > 0 && $request->price_max < $request->price){
                    return $this->dataError("Giá kết thúc phải lớn hơn giá bắt đầu hoặc bằng 0.",[],200);
              }
              $title = $request->title;
              $slug = unique_slug($this->convert_slug($title));
              $day  = '+'.$request->day_end.' days';
              $data = [
                  'title'             => $request->title,
                  'slug'              => $slug,
                  'description'       => $request->ad_description,
                  'category_id'       => $request->category_id,
                  'type'              => $request->type,
                  'price'             => $request->price,

                  'seller_name'       => Auth::user()->name,
                  'seller_email'      => Auth::user()->email,
                  'seller_phone'      => Auth::user()->phone,
                  'country_id'        => Auth::user()->country_id,
                  'address'           => Auth::user()->address,

                  'category_type'     => 'auction',
                  'status'            => '0',
                  'user_id'           => Auth::user()->id,
                  'expired_at'        => date("Y-m-d H:i:s", strtotime($day)),
                  'next_min'        => $request->step,
                  'buy_now'        => $request->price_max,
              ];


              $auctions = Ad::create($data);

              if (get_option('ads_moderation') == 'direct_publish'){
                  $data['status'] = '1';
              }

              if ( ! $request->price_plan){
                  $data['price_plan'] = 'regular';
              }


              $this->uploadAdsImage($request,$auctions->id);



              return $this->dataSuccess('Tạo đấu giá thành công',$auctions,200);

          }
          catch (\Exception $exception)
          {
              return $this->dataError($exception->getMessage(),null,200);
          }


      }

      public function uploadAdsImage(Request $request, $ad_id = 0){
          $user_id = 0;

          if (Auth::check()){
              $user_id = Auth::user()->id;
          }

          if ($request->hasFile('images')){
              $images = $request->file('images');
              foreach ($images as $image){
                  $valid_extensions = ['jpg','jpeg','png'];
                  if ( ! in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions) ){
                      return redirect()->back()->withInput($request->input())->with('error', 'Only .jpg, .jpeg and .png is allowed extension') ;
                  }

                  $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
                  $resized = Image::make($image)->resize(640, null, function ($constraint) {
                      $constraint->aspectRatio();
                  })->stream();
                  $resized_thumb = Image::make($image)->resize(320, 213)->stream();

                  $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

                  $imageFileName = 'uploads/images/'.$image_name;
                  $imageThumbName = 'uploads/images/thumbs/'.$image_name;

                  try{
                      //Upload original image
                      $is_uploaded = current_disk()->put($imageFileName, $resized->__toString(), 'public');

                      if ($is_uploaded) {
                          //Save image name into db
                          $created_img_db = Media::create(['user_id' => $user_id, 'ad_id' => $ad_id, 'media_name'=>$image_name, 'type'=>'image', 'storage' => get_option('default_storage'), 'ref'=>'ad']);

                          //upload thumb image
                          current_disk()->put($imageThumbName, $resized_thumb->__toString(), 'public');
                          $img_url = media_url($created_img_db, false);
                      }
                  } catch (\Exception $e){
                      return redirect()->back()->withInput($request->input())->with('error', $e->getMessage()) ;
                  }
              }
          }
      }
}
