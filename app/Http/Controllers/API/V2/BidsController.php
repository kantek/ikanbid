<?php

namespace App\Http\Controllers\API\V2;

use App\Ad;
use App\Bid;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class BidsController extends Controller
{

    public function store(Request $request,$id)
    {
        try{
            $ads = Ad::find($id);
            $curent_bid = Bid::where('bids.ad_id','=',$id)->orderby('bids.id','DESC')->first();
            // print_r($curent_bid->bid_amount);die;
            if($curent_bid->is_accepted == 1){
                return $this->dataError("Đấu giá đã kết thúc.",null,200);
            }
            if($curent_bid->bid_amount + $ads->next_min > $request->bid_amount){
                return $this->dataErrorBids("Đấu giá tiếp theo phải lớn hơn hoặc bằng :",$curent_bid->bid_amount + $ads->next_min,[],200);
            }
            if($request->bid_amount >= $ads->buy_now ){
                  $bid = new Bid();
                  $bid->user_id = Auth::user()->id;
                  $bid->ad_id = $id;
                  $bid->is_accepted = 1;
                  $bid->bid_amount = $request->bid_amount;
                  $bid->save();
                  $ads->expired_at = date("Y-m-d", strtotime("-1 days"));
                  $ads->save();
                  return $this->dataSuccessBid('Đấu giá thành công',$bid,200,true);
            }else{

                $bid = new Bid();
                $bid->user_id = Auth::user()->id;
                $bid->ad_id = $id;
                $bid->is_accepted = 0;
                $bid->bid_amount = $request->bid_amount;
                $bid->save();
                return $this->dataSuccessBid('Đấu giá thành công',$bid,200,false);
            }



        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }

    }

    public function show($id)
    {
          try{
              $bids = Bid::orderby('bids.id','DESC');
              $bids = $bids->join('users', 'users.id', '=', 'bids.user_id');
              $bids = $bids->where('bids.ad_id','=',$id);
              return $this->dataSuccess('Lấy đấu giá chi tiết thành công',$bids->get(),200);

          }
          catch (\Exception $exception)
          {
              return $this->dataError($exception->getMessage(),null,200);
          }
    }


    public function testPush(Request $request)
    {
       try{
           \FirebaseService::fcm($request->device_token,'Test Push','Push thành công.',[],'test_push');
           return $this->dataSuccess('Push thành công',null,200);
       }
       catch (\Exception $exception)
       {
           return $this->dataError($exception->getMessage(),null,200);
       }
    }

}
