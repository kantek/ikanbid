<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try
        {

            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'phone' => 'required|numeric',
                    'email' => 'required',
                    'password' => 'required',
                    'device_token' => 'required',
                    'address_receive_good' => 'required'
                ],[
                  'name.required' => 'Tên không được để trống',
                  'phone.required' => 'Số phone không được để trống',
                  'phone.numeric' => 'Số điện thoại phải là số',
                  'email.required' => 'Email không được để trống',
                  'password.required' => 'Mật khẩu không được để trống',
                  'device_token.required' => 'Device token không được để trống',
                  'address_receive_good.required' => 'Address receive good không được để trống'
                ]
            );

            if ($validator->fails()) {
                return $this->dataError($validator->errors(),null,200);
            }

            $user_request = User::where('email',$request->email)->first();

            if($user_request)
            {
                return $this->dataError('Email đã tồn tại',null,200);
            }



            $user = new User();
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->device_token = $request->device_token;
            $user->address_receive_good = $request->address_receive_good;
            if ($request->hasFile('photo')){
                $rules = ['photo'=>'mimes:jpeg,jpg,png'];
                $this->validate($request, $rules);

                $image = $request->file('photo');
                $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
                $resized_thumb = Image::make($image)->resize(300, 300)->stream();

                $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

                $imageFileName = 'uploads/avatar/'.$image_name;

                //Upload original image
                $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');

                if ($is_uploaded){
                    $previous_photo= $user->photo;
                    $previous_photo_storage= $user->photo_storage;

                    $user->photo = $image_name;
                    $user->photo_storage = get_option('default_storage');
                    $user->save();

                    if ($previous_photo){
                        $previous_photo_path = 'uploads/avatar/'.$previous_photo;
                        $storage = Storage::disk($previous_photo_storage);
                        if ($storage->has($previous_photo_path)){
                            $storage->delete($previous_photo_path);
                        }
                    }
                }
            }
            $user->save();

            $token = $user->createToken('REGISTER')->accessToken;
            $user['token'] = $token;

            return $this->dataSuccess('Đăng kí thành công',$user,200);
        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),null,200);
        }
    }

    public function login(Request $request)
    {
        try
        {

            $validator = \Validator::make($request->all(), [

                'email'    => 'required',
                'password' => 'required'
            ], [
                'email.required'    => 'Email bắt buộc',
                'password.required' => 'Password bắt buộc nhập'
            ]);

            if($validator->fails()) {
                return $this->dataError('lỗi xác thực', $validator->errors(), 200);
            }



            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = User::where('email',$request->email)->first();
                $user->device_token = $request->device_token;
                $user->save();
                Auth::login($user);
                $user = Auth::user();
                $token = $user->createToken('LOGIN')->accessToken;
                $user['token'] = $token;

                return $this->dataSuccess('Đăng nhập thành công',$user,200);
            }
            else
            {
                return $this->dataError('Tên đăng nhập hoặc mật khẩu không đúng',null,200);
            }
        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }
    }

    public function logout(){
        try{
            $accessToken = Auth::user()->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
                ]);
            $accessToken->revoke();

            return $this->dataSuccess('Đăng xuất thành công',null,200);

        }catch (\Exception $exception){
            $this->dataError('Đăng xuất thất bại',null,200);
        }

    }

    public function update(Request $request)
    {

        try
        {

            $user = User::where('id',Auth::user()->id)->first();
            $user->name = $request->name ? $request->name : $user->name;
            $user->phone = $request->phone ? $request->phone : $user->phone;
            $user->address_receive_good = $request->address_receive_good ? $request->address_receive_good : $user->address_receive_good;

            if ($request->hasFile('photo')){
                $rules = ['photo'=>'mimes:jpeg,jpg,png'];
                $this->validate($request, $rules);

                $image = $request->file('photo');
                $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
                $resized_thumb = Image::make($image)->resize(300, 300)->stream();

                $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

                $imageFileName = 'uploads/avatar/'.$image_name;

                //Upload original image
                $is_uploaded = current_disk()->put($imageFileName, $resized_thumb->__toString(), 'public');

                if ($is_uploaded){
                    $previous_photo= $user->photo;
                    $previous_photo_storage= $user->photo_storage;

                    $user->photo = $image_name;
                    $user->photo_storage = get_option('default_storage');
                    $user->save();

                    if ($previous_photo){
                        $previous_photo_path = 'uploads/avatar/'.$previous_photo;
                        $storage = Storage::disk($previous_photo_storage);
                        if ($storage->has($previous_photo_path)){
                            $storage->delete($previous_photo_path);
                        }
                    }
                }
            }

            $user->save();

            return $this->dataSuccess('Đăng kí thành công',$user,200);
        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),null,200);
        }

    }


    public function facebook(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'fb_id' => 'required',
            ],[

                    'email.required' => 'Email không được để trống',
                    'fb_id.required' => 'Facebook ID không được để trống',

                ]
            );

            if ($validator->fails()) {
                return $this->dataError($validator->errors(),null,200);
            }

            if($request->email || $request->fb_id)
            {
                $user = User::where('email',$request->email)->orWhere('fb_id',$request->fb_id)->first();
                if($user)
                {
                    $user->device_token = $request->device_token;
                    if(!$user->photo){
                        $user->photo = $request->photo ? $request->photo : $user->photo;
                    }

                    $user->save();
                    Auth::login($user);
                    $user = Auth::user();
                    $token = $user->createToken('LOGIN')->accessToken;
                    $user['token'] = $token;

                    return $this->dataSuccess('Đăng nhập thành công',$user,200);
                }
            }

            $user = new User();
            $user->name  = $request->name;
            $user->email  = $request->email;
            $user->photo = $request->photo ? $request->photo : null;
            $user->device_token = $request->device_token;
            $user->phone = $request->phone ? $request->phone : null;
            $user->user_type = 'user';
            $user->save();

            Auth::login($user);
            $user = Auth::user();
            $token = $user->createToken('LOGIN')->accessToken;
            $user['token'] = $token;

            return $this->dataSuccess('Đăng nhập thành công',$user,200);


        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),null,200);
        }
    }

    public function google(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'gg_id' => 'required',
            ],[

                    'email.required' => 'Email không được để trống',
                    'gg_id.required' => 'Google ID không được để trống',

                ]
            );

            if ($validator->fails()) {
                return $this->dataError($validator->errors(),null,200);
            }

            if($request->email || $request->gg_id)
            {
                $user = User::where('email',$request->email)->orWhere('gg_id',$request->gg_id)->first();
                if($user)
                {
                    $user->device_token = $request->device_token;
                    if(!$user->photo){
                        $user->photo = $request->photo ? $request->photo : $user->photo;

                    }
                    $user->save();
                    Auth::login($user);
                    $user = Auth::user();
                    $token = $user->createToken('LOGIN')->accessToken;
                    $user['token'] = $token;

                    return $this->dataSuccess('Đăng nhập thành công',$user,200);
                }
            }

            $user = new User();
            $user->name  = $request->name;
            $user->gg_id  = $request->gg_id;
            $user->photo = $request->photo ? $request->photo : null;
            $user->device_token = $request->device_token;
            $user->phone = $request->phone ? $request->phone : null;
            $user->email = $request->email;
            $user->user_type = 'user';
            $user->save();

            Auth::login($user);
            $user = Auth::user();
            $token = $user->createToken('LOGIN')->accessToken;
            $user['token'] = $token;

            return $this->dataSuccess('Đăng nhập thành công',$user,200);


        }catch (\Exception $exception){
            return $this->dataError($exception->getMessage(),null,200);
        }
    }
}
