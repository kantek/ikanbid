<?php

namespace App\Http\Controllers\API\V1;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getList()
    {
        try{

            $categories = Category::all();

            return $this->dataSuccess('Lấy danh mục thành công',$categories,200);
        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }
    }
}
