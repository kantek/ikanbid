<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'API\V1'], function() {
    Route::group(['prefix' => 'auth'],function(){
        Route::post('/register','Auth\AuthController@register');
        Route::post('/login','Auth\AuthController@login');
        Route::post('/facebook','Auth\AuthController@facebook');
        Route::post('/google','Auth\AuthController@google');

    });

    Route::post('/home','HomeController@home');

    Route::group(['prefix' => 'auth','middleware' => 'auth:api'],function(){
        Route::post('/logout','Auth\AuthController@logout');

    });

    Route::group(['prefix' => 'auctions','middleware' => 'auth:api'],function(){
        Route::post('/create','AuctionController@create');
        Route::post('/bid','AuctionController@bid');

    });

    Route::get('/category/list','CategoryController@getList');

    Route::group(['prefix' => 'auctions'],function(){
        Route::get('/search/{category_id?}/{name?}','AuctionController@search');
        Route::get('/detail/{id}','AuctionController@getDetail');

        Route::post('/users','UsersAuctionControler@getAuctionByUsers');
    });
    Route::group(['prefix' => 'bids'],function(){
        Route::post('/getByUserId','UsersBidControler@getBidByUsers');
    });




});

// Luu Nguyen Control Api Start
Route::group(['namespace' => 'API\V2'], function() {

    // Auction create, update, delete and sold
    Route::group(['prefix' => 'auctions' , 'middleware' => 'auth:api'],function(){
        Route::post('/', 'AuctionController@store');
    });

    // Auction search , detail
    Route::group(['prefix' => 'auctions'],function(){
        Route::get('/', 'AuctionController@index');
        Route::get('/{id}', 'AuctionController@show');
    });

    // Auction place bid
    Route::group(['prefix' => 'bids','middleware' => 'auth:api'],function(){
        Route::post('/{id}', 'BidsController@store');
    });

    // Get bids
    Route::group(['prefix' => 'bids'],function(){
        Route::get('/{id}', 'BidsController@show');
    });

    Route::post('test-push','BidsController@testPush');
});
